package main

//#include "print.h"
import "C"
import "fmt"

func main() {
	// leet is a C int type
	leet := C.assemblyLeet()
	C.execPrint(leet)
	
	// leetGo is a Go int type
	leetGo := int(leet)
	fmt.Println("Some other golang print, with same assembly value: ", leetGo)
}
