#include <stdio.h>
#include <inttypes.h>

int assemblyLeet() {
  int64_t a=10, b=300, output;
  // https://gcc.gnu.org/onlinedocs/gcc/extensions-to-the-c-language-family/how-to-use-inline-assembly-language-in-c-code.html
  // https://www.ibiblio.org/gferg/ldp/GCC-Inline-Assembly-HOWTO.html
  asm(
	"push %1\n"
	"pop %%rax\n"
	"and $15,%%rax\n"
	"imul $100,%%rax\n"
    "xor %%rbx,%%rbx\n" // clearing 64 bit base register, no needed for anything, obfuscation
    "add $3,%%rax\n"
    "jmp .+0x3\n"       // jumping 3 bytes after
    "nop\n"             // no operation
    "nop\n"             // no operation
	"nop\n"             // no operation
    "add %2,%%rax\n"
	"jmp loop\n"
    "lea (%%rdi),%%rax\n" // storing address at destination index into accumulator register
	"loop:\n"
	"inc %%rax\n"
	"cmp $0x539,%%rax\n"
	"jl loop\n"       // "jump when less than" to "loop" label
    "mov %%rax,%0\n"
//    "mov $24,%%rax\n"   // SSN 0x18 NtAllocateVirtualMemory
//    "xor %%rax,%%rax\n"
//    "add $1,%%ah\n"  // add 0001 to left part of 16 bits
//    "add $8,%%al\n"  // add 1000 to the right part of 16 bits, since all Accumulator was cleared to zero now holds 11000 = 24 = 0x18
//    "syscall\n"
    :"=r"(output) // %0: Output variable
    :"r"(a), "r"(b) // %1 and %2: Input variable list
    :"%rax", "%rbx", "%rdi" // Used registers ("Clobber list")
    /* 
	If handling with pointers you need to match CPU architecture, so if it is 64 all r registers
	Note that registers and variables must match number of bits:
      ================ rax (64 bits)
              ======== eax (32 bits)
                  ====  ax (16 bits)
                  ==    ah (8 bits)
                    ==  al (8 bits)
    */
  );
  return output;
}

void execPrint(int value) {
    printf("Executed printf correctly from C, with value from Assembly: %i\n", value);
}

// Test compiling with gcc directly, also to debug better or objdump
/*int main() {
	execPrint(assemblyLeet());
	return 0;
}

00000000004015b0 <assemblyLeet>:
  4015b0:       55                      push   %rbp
  4015b1:       57                      push   %rdi
  4015b2:       53                      push   %rbx
  4015b3:       48 89 e5                mov    %rsp,%rbp
  4015b6:       48 83 ec 20             sub    $0x20,%rsp
  4015ba:       48 c7 45 f8 0a 00 00    movq   $0xa,-0x8(%rbp)
  4015c1:       00
  4015c2:       48 c7 45 f0 2c 01 00    movq   $0x12c,-0x10(%rbp)
  4015c9:       00
  4015ca:       48 8b 55 f8             mov    -0x8(%rbp),%rdx
  4015ce:       48 8b 4d f0             mov    -0x10(%rbp),%rcx
  4015d2:       52                      push   %rdx
  4015d3:       58                      pop    %rax
  4015d4:       48 83 e0 0f             and    $0xf,%rax
  4015d8:       48 6b c0 64             imul   $0x64,%rax,%rax
  4015dc:       48 31 db                xor    %rbx,%rbx
  4015df:       48 83 c0 03             add    $0x3,%rax
  4015e3:       eb 01                   jmp    4015e6 <assemblyLeet+0x36>
  4015e5:       90                      nop
  4015e6:       90                      nop
  4015e7:       90                      nop
  4015e8:       48 01 c8                add    %rcx,%rax
  4015eb:       eb 03                   jmp    4015f0 <loop>
  4015ed:       48 8d 07                lea    (%rdi),%rax
  
00000000004015f0 <loop>:
  4015f0:       48 ff c0                inc    %rax
  4015f3:       48 3d 39 05 00 00       cmp    $0x539,%rax
  4015f9:       7c f5                   jl     4015f0 <loop>
  4015fb:       48 89 c2                mov    %rax,%rdx
  4015fe:       48 89 55 e8             mov    %rdx,-0x18(%rbp)
  401602:       48 8b 45 e8             mov    -0x18(%rbp),%rax
  401606:       48 83 c4 20             add    $0x20,%rsp
  40160a:       5b                      pop    %rbx
  40160b:       5f                      pop    %rdi
  40160c:       5d                      pop    %rbp
  40160d:       c3                      retq

*/